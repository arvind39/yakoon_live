<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/images/fav.ico" type="image/x-icon">
    <title>Yakoon</title>

    <!-- Bootstrap and Custom style-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/font-awesome-4.7.0/css/font-awesome.min.css"/>
     <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick-theme.css"/>
     <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/aos.css"/> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css"/>   
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/responsive-style.css"/>

<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/fluidplayer.min.js"></script>

    <?php wp_head();?>
   </head>
  <body>
         <!-- Header start -->  
         <header class="menu-bar">
            
           <nav class="navbar navbar-expand-lg container">
           
             <!-- Logo and Menu list -->
            
            <div class="navbar-brand">
            <a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="logo"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
            </button>
            </div>
            <a href="https://www.yakoon.sa" class="language_link d-lg-none ">
                  English 
                  <!-- <img src="https://www.yakoon.sa/ar/wp-content/uploads/2021/08/united-states.png"> -->
                </a>
                
              <div class="menu-list collapse navbar-collapse justify-content-end" id="navbarNav">
              <a href="https://www.yakoon.sa" class="language_link d-none d-lg-block">
                  English 
                  <!-- <img src="https://www.yakoon.sa/ar/wp-content/uploads/2021/08/united-states.png"> -->
                </a>
                <?php 
                  /* Primary navigation */
                      wp_nav_menu( array(
                    'menu' => 'menu1',
                      'depth' => 8,
                    'container' => false,
                    'menu_class' => 'navbar-nav',
                      //Process nav menu using our custom nav walker
                      'walker' => new wp_bootstrap_navwalker())
                    );
                  ?>      
            </div>
        
            
            
            <!-- Logo and menu close -->
           <?php //echo do_shortcode('[google-translator]');?>
           
          </nav>

          <!-- navigation bar close -->
          </header> 